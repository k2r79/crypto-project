import utils.Analyzer;
import utils.FileIO;
import utils.Normalizer;

import java.io.IOException;

public class Main
{
    public static void main(String[] args)
    {
        FileIO file = new FileIO("/Users/Vincent/Desktop/fichier.txt");
        String text = "";
        try {
            file.open();
            text = Normalizer.normalize(file.read());
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(text);
        System.out.println("Langue du texte : " + Analyzer.checkLanguage(text));
        System.out.println();

//        Cesar cesar = new Cesar(text);
//        Object[][] statistics = cesar.analyze(Analyzer.commonEnglishLetters);
//
//        for (int statisticsIndex = 0; statisticsIndex < statistics[0].length; statisticsIndex++) {
//            System.out.println(statistics[0][statisticsIndex] + " => " + statistics[1][statisticsIndex]);
//        }

//        Permutation permutation = new Permutation(text);
//        permutation.analyze(Analyzer.commonEnglishLetters);

//        Polybe polybe = new Polybe(text);
//
//        PolybeCodeBreaker polybeCodeBreaker = new PolybeCodeBreaker(polybe.encrypt());
//        polybe.setText(polybe.encrypt());
//        System.out.println(polybe.decrypt());

//        Triangulaire triangulaire = new Triangulaire(text, "CLE");
    }
}
