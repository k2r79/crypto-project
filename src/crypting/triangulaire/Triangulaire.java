package crypting.triangulaire;

import utils.Analyzer;
import utils.Normalizer;

public class Triangulaire
{
    private String text;
    private String key;

    public Triangulaire(String text, String key)
    {
        this.text = text;
        this.key = key;
    }

    public String[] encrypt()
    {
        String normalizedString = Normalizer.normalize(text);
        String normalizedKey = Normalizer.normalize(key);

        // Compute the tables height and max length
        int tableLength = 1;
        int tableHeight = 1;
        int length = normalizedString.length();
        while (length > 0) {
            length -= tableLength;

            if (tableHeight == 2)
            {
                tableLength++;
            } else {
                tableLength += 2;
            }

            tableHeight++;
        }

        // Complete the normalized string
        int finalLength = normalizedString.length() + length + (length < 0 ? (tableLength + 2) / 2 : tableLength / 2);
        char[] originalNormalizedTable = normalizedString.toCharArray();
        char[] normalizedTable = new char[finalLength];
        for (int textIndex = 0; textIndex < finalLength; textIndex++) {
            normalizedTable[textIndex] = textIndex >= normalizedString.length() ? randomLetter() : originalNormalizedTable[textIndex];
        }

        // Divide the inputed text
        int normalizedIndex = normalizedTable.length - 1;
        String[] cryptedTable = new String[tableLength + 1];
        for (int level = 0; level <= tableHeight; level++) {
            for (int i = tableLength - level; i >= level; i -= 2) {
                cryptedTable[i] = normalizedTable[normalizedIndex] + (cryptedTable[i] != null ? cryptedTable[i] : "");
                normalizedIndex--;
            }
        }

        // Repeat Key if necessary
        char[] repeatedKey = getRepeatedKey(normalizedKey, cryptedTable.length);

        // Order the columns with key
        int repeatedKeyIndex = 0;
        String[] finalCryptedTable = new String[cryptedTable.length];
        for (int number : getOrderFromKey(repeatedKey)) {
            finalCryptedTable[number] = cryptedTable[repeatedKeyIndex];
            repeatedKeyIndex++;
        }

        return finalCryptedTable;
    }

    public String encryptAsString()
    {
        String cryptedString = "";
        String[] cryptedTable = encrypt();

        for (String column : cryptedTable) {
            cryptedString += column;
        }

        return cryptedString;
    }

    public String decryptAsString()
    {
        String normalizedString = Normalizer.normalize(text);
        String normalizedKey = Normalizer.normalize(key);

        // Compute the tables height and max length
        int tableLength = 1;
        int tableHeight = 1;
        int length = normalizedString.length();
        while (length > 0) {
            length -= tableLength;

            if (tableHeight == 2) {
                tableLength++;
            } else {
                tableLength += 2;
            }

            tableHeight++;
        }

        // Cell length table
        int[] cellLengthTable = new int[tableLength + 1];
        int halfOfTableLength = cellLengthTable.length / 2;
        int halfOfTableHeight = tableHeight / 2;
        int cellLengthIteration = 0;
        for (int cellLengthIndex = 0; cellLengthIndex < halfOfTableLength; cellLengthIndex += 2) {
            int currentInferiorIndex = halfOfTableLength - cellLengthIndex;
            int currentSuperiorIndex = halfOfTableLength + cellLengthIndex;

            cellLengthTable[currentInferiorIndex] = cellLengthTable[currentSuperiorIndex] = cellLengthTable[currentInferiorIndex - 1] = cellLengthTable[currentSuperiorIndex + 1] = halfOfTableHeight - cellLengthIteration;

            cellLengthIteration++;
        }

        // Repeat Key if necessary
        char[] repeatedKey = getRepeatedKey(normalizedKey, tableLength + 1);

        // Extract letters from crypted string to a table
        String[] decryptedTable = new String[cellLengthTable.length];
        int[] orderTable = getOrderFromKey(repeatedKey);
        int stringIndex = 0;
        int stringIteratorIndex;
        for (int decryptedTableIndex = 0; decryptedTableIndex < decryptedTable.length; decryptedTableIndex++) {
            int stringLength = cellLengthTable[getIndexInIntArray(orderTable, decryptedTableIndex)];
            decryptedTable[decryptedTableIndex] = "";
            for (stringIteratorIndex = stringIndex; stringIteratorIndex < stringIndex + stringLength; stringIteratorIndex++) {
                decryptedTable[decryptedTableIndex] += normalizedString.toCharArray()[stringIteratorIndex];
            }

            stringIndex = stringIteratorIndex;
        }

        // Rebuild the original triangle with reversed letters
        String[] originalTriangle = new String[decryptedTable.length];
        for (int originalTriangleIndex = 0; originalTriangleIndex < originalTriangle.length; originalTriangleIndex++) {
            String decryptedTableString = decryptedTable[orderTable[originalTriangleIndex]];
            originalTriangle[originalTriangleIndex] = "";
            for (int decryptedTableStringIndex = decryptedTableString.length() - 1; decryptedTableStringIndex >= 0; decryptedTableStringIndex--) {
                originalTriangle[originalTriangleIndex] += decryptedTableString.toCharArray()[decryptedTableStringIndex];
            }
        }

        // Order the output from table
        String decryptedString = "";
        int originalTriangleStringIndex = 0;
        for (int originalTriangleOffset = 0; originalTriangleOffset <= halfOfTableLength; originalTriangleOffset++) {
            for (int i = halfOfTableLength - originalTriangleOffset; i <= halfOfTableLength + originalTriangleOffset; i += 2) {
                decryptedString += originalTriangle[i].toCharArray()[halfOfTableHeight - 1 - originalTriangleStringIndex];
            }

            if (originalTriangleOffset != 0 && originalTriangleOffset % 2 == 1)
            {
                originalTriangleStringIndex++;
            }
        }

        return decryptedString;
    }

    private int getIndexInIntArray(int[] orderKey, int value)
    {
        for (int orderKeyIndex = 0; orderKeyIndex < orderKey.length; orderKeyIndex++)
        {
            if (orderKey[orderKeyIndex] == value) {
                return orderKeyIndex;
            }
        }

        return -1;
    }

    private char randomLetter() {
        char[] charArray = Analyzer.letters.toCharArray();
        return charArray[((int)(Math.random() * (Analyzer.letters.length() - 1)))];
    }

    private char[] getRepeatedKey(String normalizedKey, int length)
    {
        // Repeat the key if necessary
        char[] repeatedKey = new char[length];
        char[] keyTable = normalizedKey.toCharArray();
        for (int keyIndex = 0; keyIndex < length; keyIndex++) {
            repeatedKey[keyIndex] = keyTable[keyIndex % (normalizedKey.length())];
        }

        return repeatedKey;
    }

    public int[] getOrderFromKey(char[] repeatedKey)
    {
        int index = 0;
        int[] orderTable = new int[repeatedKey.length];
        for (char letter : Analyzer.letterTable) {
            for (int keyIndex = 0; keyIndex < orderTable.length; keyIndex++) {
                if (repeatedKey[keyIndex] == letter) {
                    orderTable[keyIndex] = index++;
                }
            }
        }

        return orderTable;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }
}
