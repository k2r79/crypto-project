package crypting.polybe;

import utils.Analyzer;
import utils.Normalizer;

/**
 * Classe gérant l'encodage et décodage avec le code
 */
public class Polybe
{
    private String text;
    private String key;
    private char[][] polybiusSquare = new char[5][5];

    public Polybe(String text)
    {
        this.text = text;
        this.key = Analyzer.letters;
        generatePolybiusSquare();
    }

    public Polybe(String text, String key)
    {
        this.text = text;
        this.key = key;
        generatePolybiusSquare();
    }

    private void generatePolybiusSquare()
    {
        int letterIndex = 0;
        for (int xIndex = 0; xIndex < polybiusSquare.length; xIndex++) {
            for (int yIndex = 0; yIndex < polybiusSquare[0].length; yIndex++) {
                if (key.toCharArray()[letterIndex] == 'W') {
                    letterIndex++;
                }

                polybiusSquare[xIndex][yIndex] = key.toCharArray()[letterIndex];

                letterIndex++;
            }
        }
    }

    public String encrypt() {

        String normalizedString = Normalizer.normalize(Normalizer.replaceLetter(text, 'w', 'v'));
        String encryptedString = "";
        for (char clearLetter : normalizedString.toCharArray())
        {
            encryptedString += indexInPolybiusSquare(clearLetter);
        }
        return encryptedString;
    }

    private String indexInPolybiusSquare(char letter)
    {
        for (int xIndex = 0; xIndex < polybiusSquare.length; xIndex++) {
            for (int yIndex = 0; yIndex < polybiusSquare[0].length; yIndex++) {
                if (polybiusSquare[xIndex][yIndex] == letter) {
                    return String.valueOf(xIndex + 1) + String.valueOf(yIndex + 1);
                }
            }
        }

        return null;
    }

    /**
     * decrypt an encoded text with the Polybe method by cutting them into couple of values and search the letter that correspond
     * @return
     */
    public String decrypt() {
        int i = 0;

        String decryptedtext ="";

        do{
            String theEncodeLetter = text.substring(i,i+2);
            decryptedtext += polybiusSquare[Character.getNumericValue(theEncodeLetter.toCharArray()[0]) - 1][Character.getNumericValue(theEncodeLetter.toCharArray()[1]) - 1];
            i=i+2;
        }while (i+2<=text.length());
        return decryptedtext;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
        generatePolybiusSquare();
    }
}
