package crypting.permutation;

import utils.Analyzer;
import utils.Normalizer;
import utils.Sorter;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Permutation
{
    private String cryptedString = new String();
    private char[] cryptedTable = new char[Analyzer.letters.length()];
    private String text;
    private String key;
    private char[] keyTable;

    public Permutation(String text)
    {
        this.text = text;
    }

    public Permutation(String text, String key)
    {
        this.text = text;
        this.key = key;
        this.keyTable = key.toCharArray();
    }

    public String encrypt()
    {
        String normalizedString = Normalizer.normalize(text);

        String encryptedString = "";
        for (char clearLetter : normalizedString.toCharArray())
        {
            encryptedString += keyTable[Analyzer.letters.indexOf(clearLetter)];
        }

        return encryptedString;
    }

    public String decrypt()
    {
        String normalizedString = Normalizer.normalize(text);

        String decryptedString = "";
        for (char clearLetter : normalizedString.toCharArray())
        {
            decryptedString += key.indexOf(clearLetter) > -1 ? Analyzer.letterTable[key.indexOf(clearLetter)] : clearLetter;
        }

        return decryptedString;
    }

    /**
     * Generates a crypted letter table
     */
    private void generateCryptedString()
    {
        char chosenLetter;
        for (int i = 0; i < Analyzer.letterTable.length; i++)
        {
            do
            {
                chosenLetter = Analyzer.letterTable[(int)(Math.random() * Analyzer.letterTable.length)];
            } while(cryptedString.indexOf(chosenLetter) > -1);

            cryptedString += chosenLetter;
        }

        cryptedTable = cryptedString.toCharArray();
    }

    /**
     * Generates the decrypting permutation table from the occurrences
     * @param occurrences
     * @return
     */
    private Map<String, String> generatePermutationTable(Map<String, Integer> occurrences, Object[][] letterStatistics)
    {
        Map<String, String> permutationTable = new HashMap<String, String>();
        Object[][] commonLetters = Analyzer.getSortedLetters(letterStatistics);
        int i = 0;
        for (Map.Entry<String, Integer> occurrence : occurrences.entrySet())
        {
            permutationTable.put(occurrence.getKey(), (String)commonLetters[0][i]);

            i++;
        }

        return permutationTable;
    }

    public void analyze(Object[][] letterStatistics)
    {
        // Count occurrences
        String normalizedCryptedText = Normalizer.normalize(text);
        HashMap<Character, Integer> singleLetterOccurrences = new HashMap<Character, Integer>();
        HashMap<Character, Integer> repeatedLetterOccurrences = new HashMap<Character, Integer>();
        HashMap<String, Integer> bigramOccurrences = new HashMap<String, Integer>();
        HashMap<String, Integer> trigramOccurrences = new HashMap<String, Integer>();

        // Single letter Occurrences
        for (char cryptedLetter : normalizedCryptedText.toCharArray()) {
            singleLetterOccurrences.put(cryptedLetter, Analyzer.countSubstringOccurences(String.valueOf(cryptedLetter), normalizedCryptedText));
        }

        // Repeated letter Occurrences
        for (char letter : Analyzer.letterTable) {
            String repeatedLetter = letter + "" + letter;
            int occurence = Analyzer.countSubstringOccurences(String.valueOf(repeatedLetter), normalizedCryptedText);

            if (occurence > 0) {
                repeatedLetterOccurrences.put(letter, occurence);
            }
        }

        // Bigram Occurrences
        for (int cursorPosition = 0; cursorPosition < normalizedCryptedText.length() - 2; cursorPosition++) {
            String bigram = normalizedCryptedText.toCharArray()[cursorPosition] + "" + normalizedCryptedText.toCharArray()[cursorPosition + 1];

            if (bigramOccurrences.containsKey(bigram)) {
                bigramOccurrences.put(bigram, bigramOccurrences.get(bigram) + 1);
            } else {
                bigramOccurrences.put(bigram, 1);
            }
        }

        // Trigram Occurrences
        for (int cursorPosition = 0; cursorPosition < normalizedCryptedText.length() - 3; cursorPosition++) {
            String trigram = normalizedCryptedText.toCharArray()[cursorPosition] + "" + normalizedCryptedText.toCharArray()[cursorPosition + 1] + "" + normalizedCryptedText.toCharArray()[cursorPosition + 2];

            if (trigramOccurrences.containsKey(trigram)) {
                trigramOccurrences.put(trigram, trigramOccurrences.get(trigram) + 1);
            } else {
                trigramOccurrences.put(trigram, 1);
            }
        }

        // Replace letters
        String cryptedTextSample = normalizedCryptedText.substring(0, normalizedCryptedText.length() < 50 ? normalizedCryptedText.length() : 50);
        Map<Character, Character> conversionTable = new HashMap<Character, Character>();

        // Replace single letters
        Map<Character, Integer> sortedSingleLetterOccurrences = Sorter.sortByValuesDescending(singleLetterOccurrences);
        Object[][] sortedCommonLetters = Analyzer.getSortedLetters(letterStatistics);
        int occurenceIndex = 0;
        for (Map.Entry<Character, Integer> occurence : sortedSingleLetterOccurrences.entrySet()) {
            System.out.println(cryptedTextSample + "...");

            conversionTable.put(occurence.getKey(), String.valueOf(sortedCommonLetters[0][occurenceIndex]).charAt(0));
            System.out.println(replaceLetters(cryptedTextSample, conversionTable) + "...");

            System.out.println(occurence.getKey() + " : " + occurence.getValue() + " occurences. Replacing with : " + String.valueOf(sortedCommonLetters[0][occurenceIndex]).charAt(0));
            System.out.println();
            occurenceIndex++;
        }

//        // Replace repeated letters
//        Map<Character, Integer> sortedRepeatedLetterOccurrences = Sorter.sortByValuesDescending(repeatedLetterOccurrences);
//        char[] commonRepeatedLetters = repeatedLetterStatistics;
//        int maxIndex = commonRepeatedLetters.length > sortedRepeatedLetterOccurrences.size() ? sortedRepeatedLetterOccurrences.size() : commonRepeatedLetters.length;
//        for (int repeatedLetterOccurrencesIndex = 0; repeatedLetterOccurrencesIndex < maxIndex; repeatedLetterOccurrencesIndex++) {
//            char letter = (Character) sortedRepeatedLetterOccurrences.keySet().toArray()[repeatedLetterOccurrencesIndex];
//            int occurrence = (Integer) sortedRepeatedLetterOccurrences.values().toArray()[repeatedLetterOccurrencesIndex];
//
//            System.out.println(cryptedTextSample + "...");
//            conversionTable.put(letter, commonRepeatedLetters[repeatedLetterOccurrencesIndex]);
//            System.out.println(replaceLetters(cryptedTextSample, conversionTable) + "...");
//
//            System.out.println(letter + "" + letter + " : " + occurrence + " occurences. Replacing with : " + commonRepeatedLetters[repeatedLetterOccurrencesIndex]);
//            System.out.println();
//        }

//        // Replace bigrams
//        Map<String, Integer> sortedBigramOccurrences = Sorter.sortByValuesDescending(bigramOccurrences);
//        Object[][] commonBigrams = Analyzer.getSortedLetters(bigramStatistics);
//        maxIndex = commonBigrams.length > sortedBigramOccurrences.size() ? sortedBigramOccurrences.size() : commonBigrams[0].length;
//        for (int bigramOccurrencesIndex = 0; bigramOccurrencesIndex < maxIndex; bigramOccurrencesIndex++) {
//            String bigram = (String) sortedBigramOccurrences.keySet().toArray()[bigramOccurrencesIndex];
//            int occurrence = (Integer) sortedBigramOccurrences.values().toArray()[bigramOccurrencesIndex];
//
//            System.out.println(cryptedTextSample + "...");
//            conversionTable.put(bigram.toCharArray()[0], ((String) commonBigrams[0][bigramOccurrencesIndex]).toCharArray()[0]);
//            conversionTable.put(bigram.toCharArray()[1], ((String) commonBigrams[0][bigramOccurrencesIndex]).toCharArray()[1]);
//            System.out.println(replaceLetters(cryptedTextSample, conversionTable) + "...");
//
//            System.out.println(bigram + " : " + occurrence + " occurences. Replacing with : " + commonBigrams[0][bigramOccurrencesIndex]);
//            System.out.println();
//        }

        // Replace trigrams
//        Map<String, Integer> sortedTrigramOccurrences = Sorter.sortByValuesDescending(trigramOccurrences);
//        Object[][] commonTrigrams = Analyzer.getSortedLetters(trigramStatistics);
//        maxIndex = commonTrigrams.length > sortedTrigramOccurrences.size() ? sortedTrigramOccurrences.size() : commonTrigrams[0].length;
//        for (int trigramOccurrencesIndex = 0; trigramOccurrencesIndex < maxIndex; trigramOccurrencesIndex++) {
//            String trigram = (String) sortedTrigramOccurrences.keySet().toArray()[trigramOccurrencesIndex];
//            int occurrence = (Integer) sortedTrigramOccurrences.values().toArray()[trigramOccurrencesIndex];
//
//            System.out.println(trigram + " => " + ((float) occurrence / (float) normalizedCryptedText.length()) * 100f + " : " + commonTrigrams[1][trigramOccurrencesIndex] + " <= " + commonTrigrams[0][trigramOccurrencesIndex]);
//            if (((float) occurrence / (float) normalizedCryptedText.length() * 100f) >= ((Float) commonTrigrams[1][trigramOccurrencesIndex] - 0.05f) && ((float) occurrence / (float) normalizedCryptedText.length() * 100f) <= ((Float) commonTrigrams[1][trigramOccurrencesIndex] + 0.05f) ) {
//                System.out.println(cryptedTextSample + "...");
//
//                conversionTable.put(trigram.toCharArray()[0], ((String) commonTrigrams[0][trigramOccurrencesIndex]).toCharArray()[0]);
//                conversionTable.put(trigram.toCharArray()[1], ((String) commonTrigrams[0][trigramOccurrencesIndex]).toCharArray()[1]);
//                conversionTable.put(trigram.toCharArray()[2], ((String) commonTrigrams[0][trigramOccurrencesIndex]).toCharArray()[2]);
//
//                System.out.println(replaceLetters(cryptedTextSample, conversionTable) + "...");
//
//                System.out.println(trigram + " : " + occurrence + " occurences. Replacing with : " + commonTrigrams[0][trigramOccurrencesIndex]);
//                System.out.println();
//            }
//        }

        // Prompt user input
        String userInput = null;
        char letterToChange = ' ';
        char replaceLetterBy = ' ';
        Scanner scanner = new Scanner(System.in);
        do {
            showOccurrences(sortedSingleLetterOccurrences, normalizedCryptedText.length(), letterStatistics);
            System.out.println();

            System.out.println(text);
            System.out.println(replaceLetters(text, conversionTable));
            System.out.println();

            System.out.println("Lettre à remplacer :");
            userInput = scanner.nextLine();
            letterToChange = userInput.toUpperCase().toCharArray()[0];

            if (!userInput.equals("exit")) {
                System.out.println("Remplacer par :");
                userInput = scanner.nextLine();
                replaceLetterBy = userInput.toUpperCase().toCharArray()[0];
            }

            conversionTable.put(letterToChange, replaceLetterBy);
            System.out.println();
        } while (!userInput.equals("exit"));

        System.out.println();
        for (int conversionTableIndex = 0; conversionTableIndex < conversionTable.size(); conversionTableIndex++) {
            System.out.println(conversionTable.keySet().toArray()[conversionTableIndex] + " <=> " + conversionTable.values().toArray()[conversionTableIndex]);
        }
    }

    private String replaceLetters(String cryptedString, Map<Character, Character> conversionTable)
    {
        String convertedString = "";
        for (Character letter : cryptedString.toCharArray()) {
            convertedString += conversionTable.containsKey(letter) ? conversionTable.get(letter) : letter;
        }

        return convertedString;
    }

    private String insertSpaces(String text)
    {
        String spacedText = "";
        for (int letterIndex = 0; letterIndex < text.length(); letterIndex++) {
            spacedText += text.charAt(letterIndex);

            if (letterIndex % 5 == 4 && letterIndex != 0)
            {
                spacedText += " ";
            }
        }

        return spacedText;
    }

    private void showOccurrences(Map<Character, Integer> singleLetterOccurrences, int textLength, Object[][] letterStatistics)
    {
        System.out.format("%13s %15s%n", "Texte", "Langue");
        System.out.println("----------------------------------");

        for (int conversionTableIndex = 0; conversionTableIndex < singleLetterOccurrences.size(); conversionTableIndex++) {
            System.out.format("%5s : %6.3f %1s %5s : %6.3f%n", singleLetterOccurrences.keySet().toArray()[conversionTableIndex], ((Integer) singleLetterOccurrences.values().toArray()[conversionTableIndex] / ((float) textLength) * 100f), " ", letterStatistics[0][conversionTableIndex], letterStatistics[1][conversionTableIndex]);
        }
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public void setKey(String key)
    {
        this.key = key;
    }
}
