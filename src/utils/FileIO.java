package utils;

import java.io.*;

public class FileIO
{
    private File file;
    private BufferedReader bufferedReader = null;
    private BufferedWriter bufferedWriter = null;

    public FileIO(String path)
    {
        this.file = new File(path.replaceAll("[/\\\\\\\\]", System.getProperty("file.separator")));

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("Erreur de création du fichier : " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public boolean open() throws IOException
    {
        bufferedReader = new BufferedReader(new FileReader(file));

        return bufferedReader.ready();
    }

    public String read()
    {
        String currentLine;
        String outputString = "";

        try {
            while ((currentLine = bufferedReader.readLine()) != null) {
                outputString += currentLine;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            bufferedReader.close();
        } catch (IOException e) {
            System.out.println("Erreur de fermeture du fichier : " + e.getMessage());
            e.printStackTrace();
        }

        return outputString;
    }

    public void write(String text)
    {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file.getAbsoluteFile());
        } catch (IOException e) {
            System.out.println("Erreur d'ouverture pour l'écriture : " + e.getMessage());
            e.printStackTrace();
        }

        assert fileWriter != null;
        bufferedWriter = new BufferedWriter(fileWriter);

        try {
            bufferedWriter.write(text);
        } catch (IOException e) {
            System.out.println("Erreur d'écriture dans le fichier : " + e.getMessage());
            e.printStackTrace();
        }

        try {
            bufferedWriter.close();
        } catch (IOException e) {
            System.out.println("Erreur de fermeture du fichier : " + e.getMessage());
            e.printStackTrace();
        }
    }
}
