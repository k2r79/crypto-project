package utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class StatisticsGenerator
{
    private String normalizedText;
    private Map<String, Integer> quadrigramOccurrences = new HashMap<String, Integer>();
    private Map<String, Integer> quintigramOccurrences = new HashMap<String, Integer>();

    public StatisticsGenerator(String filePath)
    {
        FileIO fileIO = new FileIO(filePath);
        try {
            if (fileIO.open()) {
                normalizedText = Normalizer.normalize(fileIO.read());
            }
        } catch (IOException e) {
            System.out.println("Erreur de lecture du fichier : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void extractQuadrigrams()
    {
        for (int cursorPosition = 0; cursorPosition < normalizedText.length() - 4; cursorPosition++) {
            String quadrigram = normalizedText.toCharArray()[cursorPosition] + "" + normalizedText.toCharArray()[cursorPosition + 1] + "" + normalizedText.toCharArray()[cursorPosition + 2] + "" + normalizedText.toCharArray()[cursorPosition + 3];

            if (quadrigramOccurrences.containsKey(quadrigram)) {
                quadrigramOccurrences.put(quadrigram, quadrigramOccurrences.get(quadrigram) + 1);
            } else {
                quadrigramOccurrences.put(quadrigram, 1);
            }
        }
    }

    public void extractQuintigrams()
    {
        for (int cursorPosition = 0; cursorPosition < normalizedText.length() - 5; cursorPosition++) {
            String quintigram = normalizedText.toCharArray()[cursorPosition] + "" + normalizedText.toCharArray()[cursorPosition + 1] + "" + normalizedText.toCharArray()[cursorPosition + 2] + "" + normalizedText.toCharArray()[cursorPosition + 3] + "" + normalizedText.toCharArray()[cursorPosition + 4];

            if (quintigramOccurrences.containsKey(quintigram)) {
                quintigramOccurrences.put(quintigram, quintigramOccurrences.get(quintigram) + 1);
            } else {
                quintigramOccurrences.put(quintigram, 1);
            }
        }
    }

    public Map<String, Integer> getQuadrigramOccurrences()
    {
        return quadrigramOccurrences;
    }

    public Map<String, Integer> getQuintigramOccurrences()
    {
        return quintigramOccurrences;
    }
}
