package utils;

import java.util.Arrays;
import java.util.Comparator;

public class Analyzer
{
    public static Object[][] commonFrenchLetters =
    {
        {"A",  "B",  "C",  "D",  "E",   "F",  "G",  "H",  "I",  "J",  "K",  "L",  "M",  "N",  "O",  "P",  "Q",  "R",  "S",  "T",  "U",  "V",  "W",  "X",  "Y",  "Z"},
        {9.42f,1.02f,2.64f,3.39f,15.87f,0.95f,1.04f,0.77f,8.41f,0.89f,0.00f,5.34f,3.24f,7.15f,5.14f,2.86f,1.06f,6.46f,7.90f,7.26f,6.24f,2.15f,0.00f,0.30f,0.24f,0.32f}
    };

    public static char[] commonFrenchRepeatedLetters =
    {
        'S', 'E', 'L', 'T', 'N', 'M', 'R', 'P', 'F', 'C', 'A', 'U', 'I'
    };

    public static Object[][] commonFrenchBigrams =
    {
        {"ES",  "LE",  "EN",  "DE",  "RE",  "NT",  "ON",  "ER",  "TE",  "SE",  "ET",  "EL",  "QU",  "AN",  "NE",  "OU",  "AI",  "EM",  "IT",  "ME",  "IS",  "LA",  "EC",  "TI",  "CE",  "ED",  "IE",  "RA",  "IN",  "EU",  "UR",  "CO",  "AR",  "TR",  "UE",  "TA",  "EP",  "ND",  "NS",  "PA",  "US",  "SA",  "SS"},
        {3.05f, 2.46f, 2.42f, 2.15f, 2.09f, 1.97f, 1.64f, 1.63f, 1.63f, 1.55f, 1.43f, 1.41f, 1.34f, 1.30f, 1.24f, 1.18f, 1.17f, 1.13f, 1.12f, 1.04f, 1.03f, 1.01f, 1.00f, 0.98f, 0.98f, 0.96f, 0.94f, 0.92f, 0.90f, 0.89f, 0.88f, 0.87f, 0.86f, 0.86f, 0.85f, 0.85f, 0.82f, 0.80f, 0.79f, 0.78f, 0.76f, 0.75f, 0.73f}
    };

    public static Object[][] commonFrenchTrigrams =
    {
        {"ENT", "LES", "EDE", "DES", "QUE", "AIT", "LLE", "SDE", "ION", "EME", "ELA", "RES", "MEN", "ESE", "DEL", "ANT", "TIO", "PAR", "ESD", "TDE"},
        {0.90f, 0.80f, 0.63f, 0.61f, 0.61f, 0.54f, 0.51f, 0.51f, 0.48f, 0.47f, 0.44f, 0.43f, 0.43f, 0.42f, 0.40f, 0.39f, 0.38f, 0.36f, 0.35f, 0.35f}
    };

    public static Object[][] commonEnglishLetters =
    {
        {"A",  "B",  "C",  "D",  "E",   "F",  "G",  "H",  "I",  "J",  "K",  "L",  "M",  "N",  "O",  "P",  "Q",  "R",  "S",  "T",  "U",  "V",  "W",  "X",  "Y",  "Z"},
        {8.17f,1.49f,2.78f,4.25f,13.00f,2.23f,2.02f,6.09f,6.97f,0.15f,0.77f,4.03f,2.41f,6.75f,7.51f,1.93f,0.10f,5.99f,6.33f,9.06f,2.76f,0.98f,2.36f,0.20f,1.97f,0.08f}
    };

    public static String letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static char[] letterTable = letters.toCharArray();

    /**
     * Class utilisée pour decryptage du carré de Polybe , est utilisé pour créer un tableau de Lettre représentant l'alphabet et les % d'apparition de chacune des lettres
     */
    public static class Lettre {
        private char laLettre;
        private float laFrequence;

        public Lettre(char p_lettre, float p_frequence){
            this.laLettre = p_lettre;
            this.laFrequence = p_frequence;
        }

        public char getLettre(){
            return laLettre;
        }

        public float getFrequence(){
            return laFrequence;
        }
    }

    public static Lettre[] lesLettres = new Lettre[26];

    /**
     * Bloc statique remplissant le tableau de Lettre lesLettres en utilisant le tableau commonFrenchLetters
     */
    static {
        for(int i=0 ; i<lesLettres.length; ++i){
            lesLettres[i]=new Lettre(commonFrenchLetters[0][i].toString().charAt(0),(Float)commonFrenchLetters[1][i]);
        }
        Arrays.sort(lesLettres, new Comparator<Lettre>()
        {
            @Override
            public int compare(Lettre o1, Lettre o2)
            {
                return (int) (o2.getFrequence() * 100 - o1.getFrequence() * 100);
            }
        });
    }

    public static String checkLanguage(String text)
    {
        text = Normalizer.normalize(text);

        Object[][] textStatistics = new Object[2][26];
        for (int letterIndex = 0; letterIndex < letterTable.length; letterIndex++) {
            textStatistics[0][letterIndex] = letterTable[letterIndex];
            textStatistics[1][letterIndex] = ((float) countSubstringOccurences(String.valueOf(letterTable[letterIndex]), text)) / ((float) text.length()) * 100f;
        }

        textStatistics = getSortedLetters(textStatistics);
        Object[][] frenchLetterStatistics = getSortedLetters(commonFrenchLetters);
        Object[][] englishLetterStatistics = getSortedLetters(commonEnglishLetters);

        float frenchIndicator = 0f;
        float englishIndicator = 0f;
        for (int statisticsIndex = 0; statisticsIndex < textStatistics.length; statisticsIndex++)
        {
            frenchIndicator += Math.abs(((Float) frenchLetterStatistics[1][statisticsIndex]) - ((Float) textStatistics[1][statisticsIndex]));
            englishIndicator += Math.abs(((Float) englishLetterStatistics[1][statisticsIndex]) - ((Float) textStatistics[1][statisticsIndex]));            System.out.println();
        }

        if (frenchIndicator > englishIndicator) {
            return "Anglais";
        } else {
            return "Français";
        }
    }

    public static int countSubstringOccurences(String subStr, String str)
    {
        return str.split(subStr, -1).length-1;
//        return (str.length() - str.replace(subStr, "").length()) / subStr.length();
    }

    /**
     * Computes the divergence of the percentage of occurrences of the letters in the string with the statistics of the language
     * (The smaller the better)
     * @return
     */
    public static float getFitnessIndicator(String decryptedString, Object[][] letterStatistics)
    {
        Object[] letters = letterStatistics[0];
        Object[] percentage = letterStatistics[1];

        float indicator = 0f;
        for (int i = 0; i < letters.length; i++)
        {
            int count = Analyzer.countSubstringOccurences((String)letters[i], decryptedString);
            indicator += Math.abs((Float)percentage[i] - ((float)count / (float)decryptedString.length()) * 100f);
        }

        return indicator;
    }

    public static Object[][] getSortedLetters(Object[][] letterStatistics)
    {
        Object[][] sortedLetters = letterStatistics;
        int maxIndex;
        float tmpValue;
        String tmpLetter;
        for (int i = 0; i < sortedLetters[0].length; i++)
        {
            maxIndex = i;

            for (int j = i + 1; j < sortedLetters[0].length; j++)
            {
                if ((Float)sortedLetters[1][j] > (Float)sortedLetters[1][maxIndex])
                {
                    maxIndex = j;
                }
            }

            tmpLetter = String.valueOf(sortedLetters[0][i]);
            tmpValue = (Float)sortedLetters[1][i];
            sortedLetters[0][i] = sortedLetters[0][maxIndex];
            sortedLetters[1][i] = sortedLetters[1][maxIndex];
            sortedLetters[0][maxIndex] = tmpLetter;
            sortedLetters[1][maxIndex] = tmpValue;
        }

        return sortedLetters;
    }
}
