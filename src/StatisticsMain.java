import utils.FileIO;
import utils.Sorter;
import utils.StatisticsGenerator;

import java.util.Map;

public class StatisticsMain
{
    public static void main(String[] args)
    {
        FileIO fileIO = new FileIO("/Users/Vincent/Desktop/quadrigrams.txt");

        StatisticsGenerator statisticsGenerator = new StatisticsGenerator("/Users/Vincent/Desktop/jules_verne.txt");
        statisticsGenerator.extractQuadrigrams();

        String outputStatistics = "";
        Map<String, Integer> quadrigrams = Sorter.sortByValuesDescending(statisticsGenerator.getQuadrigramOccurrences());
        for (int quadrigramIndex = 0; quadrigramIndex < quadrigrams.size(); quadrigramIndex++) {
            outputStatistics += quadrigrams.keySet().toArray()[quadrigramIndex] + " => " + quadrigrams.values().toArray()[quadrigramIndex] + "\r\n";
        }

        fileIO.write(outputStatistics);

        fileIO = new FileIO("/Users/Vincent/Desktop/quintigrams.txt");
        statisticsGenerator.extractQuintigrams();

        outputStatistics = "";
        Map<String, Integer> quintigrams = Sorter.sortByValuesDescending(statisticsGenerator.getQuintigramOccurrences());
        for (int quintigramIndex = 0; quintigramIndex < quintigrams.size(); quintigramIndex++) {
            outputStatistics += quintigrams.keySet().toArray()[quintigramIndex] + " => " + quintigrams.values().toArray()[quintigramIndex] + "\r\n";
        }

        fileIO.write(outputStatistics);
    }
}
