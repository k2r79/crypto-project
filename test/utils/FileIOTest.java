package utils;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FileIOTest
{
    private FileIO fileIO;

    @Before
    public void setUp() throws Exception
    {
        fileIO = new FileIO(FileIOTest.class.getProtectionDomain().getCodeSource().getLocation().getPath() + "/utils/crypto_text.txt");
        assertTrue(fileIO.open());
    }

    @Test
    public void testRead() throws Exception
    {
        assertEquals("Good it works !", fileIO.read());
    }
}
