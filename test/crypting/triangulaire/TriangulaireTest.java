package crypting.triangulaire;

import org.junit.Test;
import utils.Normalizer;

import static org.junit.Assert.assertEquals;

public class TriangulaireTest
{
    private Triangulaire t;
    private String stringToCipher = "LECODAGEPARTRANSPOSITIONTRIANGULAIRE";
    private String cipherKey = "CODAGE";
    private String repeatedCipherKey = "CODAGECODAGECOD";

    @org.junit.Before
    public void setUp() throws Exception
    {
        t = new Triangulaire(stringToCipher, cipherKey);
    }

    @Test
    public void testCryptingAsString() throws Exception
    {
        assertEquals("ROAARNEEOLTRSGCPSAEOTNNIGPUAIIILDRTA", t.encryptAsString());
    }

    @org.junit.Test
    public void testDecryptAsString() throws Exception
    {
        t.setText(t.encryptAsString());
        assertEquals(stringToCipher, t.decryptAsString());
    }

    @Test
    public void testGetOrderFromKey() throws Exception
    {
        int[] expectedOrder = { 2, 12, 5, 0, 10, 8, 3, 13, 6, 1, 11, 9, 4, 14, 7 };
        int[] order = t.getOrderFromKey(Normalizer.normalize(repeatedCipherKey).toCharArray());

        assertEquals(Normalizer.convertIntArrayToString(expectedOrder), Normalizer.convertIntArrayToString(order));
    }
}
