package crypting.polybe;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PolybeTest
{
    private Polybe polybe;
    private String textToCipher = "LECODAGEPARLECARREDEPOLYBE";
    private String cipherKey = "ZEBRASCDFGHIJKLMNOPQTUVWXY";

    @Before
    public void setUp() throws Exception
    {
        polybe = new Polybe(textToCipher);
    }

    @Test
    public void testEncrypt() throws Exception
    {
        assertEquals("3215133514112215411143321513114343151415413532541215", polybe.encrypt());
    }

    @Test
    public void testEncryptWithKey() throws Exception
    {
        polybe.setKey(cipherKey);

        assertEquals("3512224323152512441514351222151414122312444335551312", polybe.encrypt());
    }

    @Test
    public void testDecrypt() throws Exception
    {
        polybe.setText("3215133514112215411143321513114343151415413532541215");

        assertEquals("LECODAGEPARLECARREDEPOLYBE", polybe.decrypt());
    }

    @Test
    public void testDecryptWithKey() throws Exception
    {
        polybe.setKey(cipherKey);
        polybe.setText("3512224323152512441514351222151414122312444335551312");

        assertEquals("LECODAGEPARLECARREDEPOLYBE", polybe.decrypt());
    }
}