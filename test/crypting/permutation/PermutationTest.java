package crypting.permutation;

import crypting.permutation.Permutation;
import org.junit.Before;
import org.junit.Test;
import utils.Normalizer;

import static org.junit.Assert.assertEquals;

public class PermutationTest
{
    private Permutation permutation;
    private String textToCipher = "Good it works !";
    private String cipherKey = "ZEBRASCDFGHIJKLMNOPQTUVWXY";

    @Before
    public void setUp() throws Exception
    {
        permutation = new Permutation(textToCipher, cipherKey);
    }

    @Test
    public void testEncrypt() throws Exception
    {
        assertEquals("CLLRFQVLOHP", permutation.encrypt());
    }

    @Test
    public void testDecrypt() throws Exception
    {
        permutation.setText(permutation.encrypt());
        assertEquals(Normalizer.normalize(textToCipher), permutation.decrypt());
    }
}
