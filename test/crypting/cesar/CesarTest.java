package crypting.cesar;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CesarTest
{
    private Cesar cesar;
    private String textToCipher = "Good it works !";
    private int cipherKey = 7;

    @Before
    public void setUp() throws Exception
    {
        cesar = new Cesar(textToCipher, cipherKey);
    }

    @Test
    public void testEncrypt() throws Exception
    {
        assertEquals("NVVKPADVYRZ", cesar.encrypt());
    }

    @Test
    public void testDecrypt() throws Exception
    {
        cesar.setText(cesar.encrypt());
        assertEquals("GOODITWORKS", cesar.decrypt());
    }
}